package rockpaperscissors;

import game.GameItems;
import game.rockpaperscissors.logic.RockPaperScissorsLogic;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class to test the Rock, Paper, Scissors, logic implemented
 * @author rribeiro
 */
public class RockPaperScissorsLogicTest {
    /**
     * Test rock result
     */
    @Test
    public void testRock() {
        assertEquals(GameItems.RESULT.WIN, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.ROCK, GameItems.CHOICE.SCISSORS));
        assertEquals(GameItems.RESULT.LOSE, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.ROCK, GameItems.CHOICE.PAPER));
        assertEquals(GameItems.RESULT.TIE, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.ROCK, GameItems.CHOICE.ROCK));
    }
    /**
     * Test paper result
     */
    @Test
    public void testPaper() {
        assertEquals(GameItems.RESULT.WIN, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.PAPER, GameItems.CHOICE.ROCK));
        assertEquals(GameItems.RESULT.LOSE, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.PAPER, GameItems.CHOICE.SCISSORS));
        assertEquals(GameItems.RESULT.TIE, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.PAPER, GameItems.CHOICE.PAPER));
    }
    /**
     * Test scissors result
     */
    @Test
    public void testScissors() {
        assertEquals(GameItems.RESULT.WIN, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.SCISSORS, GameItems.CHOICE.PAPER));
        assertEquals(GameItems.RESULT.LOSE, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.SCISSORS, GameItems.CHOICE.ROCK));
        assertEquals(GameItems.RESULT.TIE, RockPaperScissorsLogic.getInstance().play(GameItems.CHOICE.SCISSORS, GameItems.CHOICE.SCISSORS));
    }
}
