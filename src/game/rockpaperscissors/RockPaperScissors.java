package game.rockpaperscissors;

import game.Game;
import game.rockpaperscissors.logic.RockPaperScissorsLogic;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import game.rockpaperscissors.strategy.PaperGameStrategy;
import game.rockpaperscissors.strategy.RandomGameStrategy;
import game.rockpaperscissors.strategy.RockGameStrategy;
import game.rockpaperscissors.strategy.ScissorsGameStrategy;

/**
 * Rock, Paper, Scissors main class to launch the game
 * @author rribeiro
 */
public class RockPaperScissors extends Application {
        private Game game;
        private Scene play;
        private Scene displayResult;
        private Text resultMessage;

	public static void main(String[] args) {
		launch(args);
	}

        /**
         * Start to draw the main stage
         */
        @Override
	public void start(Stage primaryStage) throws Exception {
                primaryStage.setTitle("Rock, Paper, Scissors");
                
                //create game
                game = new Game(RockPaperScissorsLogic.getInstance(), "You", "Computer");
                
                resultMessage = new Text();
		resultMessage.setFont(Font.font("", FontPosture.ITALIC, 18));
                
                // create play scene
		createPlayScene(primaryStage);
                
                // create display result scene
		createDisplayResultScene(primaryStage);

                //set play scene and show
                primaryStage.setScene(play);
		primaryStage.show();
	} 
        
        /**
         * Create play scene
         * 
         * @param primaryStage main stage
         */
        private void createPlayScene(Stage primaryStage){
            //create start message
            Text startMessage = new Text("Please make your selection below:");
            startMessage.setFont(Font.font("", FontPosture.ITALIC, 12));
            
            //create vbox to start message
            VBox vbox = new VBox();
            vbox.getChildren().addAll(startMessage);
            vbox.setAlignment(Pos.CENTER);
            
            // create button rock
            Button rock = new Button("  Rock  ");
            Image rockImage = new Image("/images/rock.png"); 
            rock.setGraphic(new ImageView(rockImage));
            
            // create button paper
            Button paper = new Button("  Paper  ");
            Image paperImage = new Image("/images/paper.png");
            paper.setGraphic(new ImageView(paperImage));
            
            // create button scissors
            Button scissors = new Button("  Scissors  ");
            Image scissorsImage = new Image("/images/scissors.png");
            scissors.setGraphic(new ImageView(scissorsImage));

            //create hbox to the buttons rock, paper, scissors
            HBox hbox1 = new HBox();
            hbox1.getChildren().addAll(rock, paper, scissors);
            hbox1.setAlignment(Pos.CENTER);
            hbox1.setPadding(new Insets(5, 5, 5, 5));
            hbox1.setSpacing(15);
            
            //create main vbox
            VBox main = new VBox(5);
            main.getChildren().addAll(vbox, hbox1);
            main.setAlignment(Pos.CENTER);
            main.setPadding(new Insets(15, 15, 15, 15));
            
            //create border
            BorderPane border = new BorderPane();
            border.setCenter(main);
            
            //create play scene
            play = new Scene(border, 700, 220);
            
            //create buttons action
            createButtonRockAction(primaryStage, rock);
            createButtonPaperAction(primaryStage, paper);
            createButtonScissorsAction(primaryStage, scissors);
        }
        
        /**
         * Create display result scene
         * 
         * @param primaryStage main stage
         */
        private void createDisplayResultScene(Stage primaryStage){
            // create button play again
            Button playAgain = new Button("Play Again");
            
            //create border
            BorderPane border = new BorderPane();
            VBox results = new VBox(10);
            results.getChildren().addAll(resultMessage, playAgain);
            results.setAlignment(Pos.CENTER);
            border.setCenter(results);
            
            //create display result scene
            displayResult = new Scene(border, 700, 220);
            
            //create buttons action
            createButtonPlayAgainAction(primaryStage, playAgain);
        }
        
        /**
         * Create button rock action
         * 
         * @param primaryStage main stage
         * @param rock button
         */
        private void createButtonRockAction(Stage primaryStage, Button rock){
            rock.setOnAction(e -> {game.run(new RockGameStrategy(), new RandomGameStrategy());
                                   resultMessage.setText(game.getResult());
                                   primaryStage.setScene(displayResult);
            });
        }
        
        /**
         * Create button paper action
         * 
         * @param primaryStage main stage
         * @param paper button
         */
        private void createButtonPaperAction(Stage primaryStage, Button paper){
            paper.setOnAction(e -> {game.run(new PaperGameStrategy(), new RandomGameStrategy());
                                    resultMessage.setText(game.getResult());
                                    primaryStage.setScene(displayResult);
            });
        }
        
        /**
         * Create button scissors again action
         * 
         * @param primaryStage main stage
         * @param scissors button
         */
        private void createButtonScissorsAction(Stage primaryStage, Button scissors){
            scissors.setOnAction(e -> {game.run(new ScissorsGameStrategy(), new RandomGameStrategy());
                                       resultMessage.setText(game.getResult());
                                       primaryStage.setScene(displayResult);
            });
        }
        
        /**
         * Create button play again action
         * 
         * @param primaryStage main stage
         * @param playAgain button
         */
        private void createButtonPlayAgainAction(Stage primaryStage, Button playAgain){
             playAgain.setOnAction(e -> {primaryStage.setScene(play);});
        }
}
