package game.rockpaperscissors.logic;

import game.GameItems;
import game.GameLogic;

/**
 * Implementation of the RockPaperScissors game logic
 * @author rribeiro
 */
public class RockPaperScissorsLogic implements GameLogic{
    private static RockPaperScissorsLogic instance = null;
   
    private RockPaperScissorsLogic() {}
    
    public static RockPaperScissorsLogic getInstance() {
        if(instance == null) {
            instance = new RockPaperScissorsLogic();
        }
        return instance;
    }
    
    /**
     * Play the game with players choices
     * 
     * @param player1Choice choice of player 1
     * @param player2Choice choice of player 2
     * @return result of the game
     */
    @Override
    public GameItems.RESULT play(GameItems.CHOICE player1Choice, GameItems.CHOICE player2Choice){
        GameItems.RESULT result;
        
        if((player1Choice.equals(GameItems.CHOICE.ROCK) && player2Choice.equals(GameItems.CHOICE.SCISSORS)) ||
           (player1Choice.equals(GameItems.CHOICE.PAPER) && player2Choice.equals(GameItems.CHOICE.ROCK)) ||
           (player1Choice.equals(GameItems.CHOICE.SCISSORS) && player2Choice.equals(GameItems.CHOICE.PAPER))){
            result = GameItems.RESULT.WIN;
        }
        else if(player1Choice.equals(player2Choice)){
            result = GameItems.RESULT.TIE;
        }
        else{
            result = GameItems.RESULT.LOSE;
        }
        return result;
    }
}
