package game.rockpaperscissors.strategy;

import game.GameItems;
import game.GameStrategy;

/**
 * This is the implementation of our GameStrategy interface that only chooses the SCISSORS strategy
 * @author rribeiro
 */
public class ScissorsGameStrategy implements GameStrategy{

    @Override
    public GameItems.CHOICE play() {
        return GameItems.CHOICE.SCISSORS;
    }
}
