package game.rockpaperscissors.strategy;

import game.GameItems;
import game.GameStrategy;

/**
 * This is the implementation of our GameStrategy interface that only chooses the PAPER strategy
 * @author rribeiro
 */
public class PaperGameStrategy implements GameStrategy{
    
    @Override
    public GameItems.CHOICE play() {
        return GameItems.CHOICE.PAPER;
    }
}
