package game.rockpaperscissors.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import game.GameItems;
import game.GameStrategy;

/** 
 * This is the implementation of our GameStrategy interface that chooses RANDOM as strategy
 * @author rribeiro
 */
public class RandomGameStrategy implements GameStrategy{
    @Override
    public GameItems.CHOICE play() {
        Random random = new Random();
        List<GameItems.CHOICE> choices = new ArrayList<>(Arrays.asList(GameItems.CHOICE.values()));
        return choices.get(random.nextInt(choices.size()));
    } 
}
