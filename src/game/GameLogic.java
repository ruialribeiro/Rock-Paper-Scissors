package game;

/**
 * Interface of the game logic
 * @author rribeiro
 */
public interface GameLogic {
    /**
     * Play the game with players choices
     * 
     * @param player1Choice choice of player 1
     * @param player2Choice choice of player 2
     * @return result of the game
     */
    public GameItems.RESULT play(GameItems.CHOICE player1Choice, GameItems.CHOICE player2Choice);
}