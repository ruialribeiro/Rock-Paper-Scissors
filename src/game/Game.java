package game;

/**
 * Game class with all elements to the game
 * @author rribeiro
 */
public class Game {
    private Player player1;
    private Player player2;
    private String result;
    private GameLogic gameLogic;
    
    /**
     * Constructor of the game
     * 
     * @param gameLogic interface with logic of the game
     * @param player1Name Player 1 name
     * @param player2Name Player 2 name
     */
    public Game(GameLogic gameLogic, String player1Name, String player2Name){
        this.gameLogic = gameLogic;
        this.player1 = new Player(player1Name);
        this.player2 = new Player(player2Name);
        this.result = "";
    }
    
    /**
     * Run the game
     * 
     * @param player1GameStrategy Strategy choosed by the player 1
     * @param player2GameStrategy Strategy choosed by the player 2
     */
    public void run(GameStrategy player1GameStrategy, GameStrategy player2GameStrategy){
        GameItems.CHOICE player1Choice = player1GameStrategy.play();
        GameItems.CHOICE player2Choice = player2GameStrategy.play();
        GameItems.RESULT gameLogicResult = gameLogic.play(player1Choice, player2Choice);
        result = buildResultMessage(gameLogicResult, player1Choice, player2Choice);
    }
    
    /**
     * Build the message with the result of the game
     * 
     * @param result result of the game
     * @param player1Choice choice of player 1
     * @param player2Choice choice of player 2
     * @return message with the result of the game
     */
    private String buildResultMessage(GameItems.RESULT result, GameItems.CHOICE player1Choice, GameItems.CHOICE player2Choice){
        String message = translate(result);
        StringBuilder sb = new StringBuilder();
        sb.append(player1.getName());
        sb.append(" ");
        sb.append(GameConst.PICK_MESSAGE);
        sb.append(" ");
        sb.append(player1Choice);
        sb.append("\n");
        sb.append(player2.getName());
        sb.append(" ");
        sb.append(GameConst.PICK_MESSAGE);
        sb.append(" ");
        sb.append(player2Choice);
        sb.append("\n");
        sb.append(GameConst.RESULT_PICK_MESSAGE);
        sb.append(" ");
        sb.append(message);
        return sb.toString();
    }       
    
    /**
     * Translate the result of the game to message
     * 
     * @param result result of the game
     * @return Message with result of the game
     */
    private String translate(GameItems.RESULT result){
        String message = "";
        switch (result) {
            case WIN:
                message = GameConst.WIN_MESSAGE;
                break;
            case LOSE:
                message = GameConst.LOSE_MESSAGE;
                break;
            case TIE:
                message = GameConst.TIE_MESSAGE;
                break;
        }
        return message;
    }
   
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
