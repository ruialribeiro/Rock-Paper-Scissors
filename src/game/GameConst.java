package game;
/**
 * Constants messages of game
 * @author rribeiro
 */
public class GameConst {
    public static final String WIN_MESSAGE = "You have won. Congratulations!";
    public static final String LOSE_MESSAGE = "You have lost. Better luck next time!";
    public static final String TIE_MESSAGE = "The game was a tie! Please try again.";
    public static final String PICK_MESSAGE = "picked:";
    public static final String RESULT_PICK_MESSAGE = "Result:";
}
