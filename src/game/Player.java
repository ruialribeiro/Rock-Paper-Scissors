package game;

/**
 * Player class with all the elements of the player
 * @author rribeiro
 */
public class Player {    
    private String name;

    public Player(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
