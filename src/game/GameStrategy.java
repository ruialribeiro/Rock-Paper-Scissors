package game;

/**
 * Interface with strategy pattern to allow differents game strategy implementations
 * @author rribeiro
 */
public interface GameStrategy {
    public GameItems.CHOICE play();
}
